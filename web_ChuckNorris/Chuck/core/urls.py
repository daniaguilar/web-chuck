"""core URL Configuration
"""
from django.contrib import admin
from django.conf import settings
from django.urls import path
from  . import views

urlpatterns = [
    # Paths del core
    path("", views.home, name='home'),
]


# Configuración para la carga de imágenes en el modo DEBUG
if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)