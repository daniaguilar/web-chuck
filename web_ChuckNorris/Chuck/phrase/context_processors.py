from .models import Phrase

# ? Función que genera frases dinámicos

def get_phrase(request):
    # Añadimos el filter para visulaizar los visbles
    pharse = Phrase.objects.filter(published=True).order_by('?')[:1]
    return{
        'pharse': pharse
    }
    """
    La función rescata un diccionario, filtrado por el campo publicado en True.
    y rescata un objeto de la función, de manera aleatoria.
    """