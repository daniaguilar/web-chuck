from django.apps import AppConfig


class PhraseConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'phrase'
    verbose_name = 'Gestión de Frases de Chuck'


    """
    Se costumiza para el panel del backend de la app
    """