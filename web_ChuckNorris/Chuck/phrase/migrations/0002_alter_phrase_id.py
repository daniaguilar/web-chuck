# Generated by Django 3.2.9 on 2021-11-18 20:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('phrase', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='phrase',
            name='id',
            field=models.IntegerField(auto_created=True, help_text=' Texto de máxion 30 carácteres.', primary_key=True, serialize=False, verbose_name='ID'),
        ),
    ]
