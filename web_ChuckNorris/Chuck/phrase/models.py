from django.db import models


# Create your models here.

#* Modelo  para frase.
class Phrase(models.Model):
    title = models.CharField(max_length=30, verbose_name='Título',help_text=" Texto de máxion 30 carácteres.")
    text = models.CharField(max_length=300, verbose_name='Frase',help_text=" Texto de máximo 300 carácteres.")
    author = models.CharField(max_length=40, verbose_name='Nombre del autor', default="Chuck Norris", help_text=' Por defecto "Chuck Norris".')
    create_at = models.DateTimeField(verbose_name='Fecha de creación ', auto_now_add=True)
    update_at = models.DateTimeField(verbose_name='Fecha de actualización', auto_now=True)
    published = models.BooleanField(default = False , verbose_name = 'Publicar', help_text=" Haz checking para publicar.") 
    class Meta:
        verbose_name = 'Frase'
        verbose_name_plural = 'Frases'
    def __str__(self) -> str:
        return self.title

        """
        Se rea el modelo con los campos que tenemos arriba,
        el campo publicar, es de tipo booleano que se necesita 
        checkear para  publicar en en frontend,
        a traves de la función creada en en context_proccesor.py
         
        """