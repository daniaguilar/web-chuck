from django.contrib import admin

# Registro del modelo de Pharse.
from .models import Phrase


# Configuración de Admin Pharse.
class PhraseAdmin(admin.ModelAdmin):
    readonly_fields = ('create_at', 'update_at') # campos de lectura
    search_fields = ('title', 'text')             # campos de busqueda
    list_display = ('id','title','published', 'author', 'create_at')  #campos que visualizamos
    ordering = ('id',) # Modo en el que se ordenan, en este caso de menor a mayor.


# Configuración personalizada del panel.
admin.site.register(Phrase, PhraseAdmin)

title = "Las Frases de Chuck"
subtitle = "Panel de Gestión "


"""
    Aquí le damos formato a los campos que aparecen en el panel administrador.
   
    """